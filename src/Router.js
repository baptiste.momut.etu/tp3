export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			if (document.location.pathname != path)
				window.history.pushState(null, null, path);
			this.#menuElement.querySelectorAll('a').forEach(link => {
				if (link.getAttribute('href') == path) link.classList.add('active');
				else link.classList.remove('active');
			});
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}

	static set menuElement(element) {
		this.#menuElement = element;
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
		element.addEventListener('click', function (event) {
			event.preventDefault();
			const target = event.target;
			//target.setAttribute('class', 'active');
			const href = target.getAttribute('href');
			console.log(href);
			Router.navigate(href);
		});
	}
}
