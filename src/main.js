import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

window.onpopstate = function (event) {
	console.log(window.history);
	Router.navigate(document.location.pathname);
};

pizzaList.pizzas = data;
Router.navigate(document.location.pathname); // affiche la liste des pizzas

document.querySelector(
	'.logo'
).innerHTML += `<small>les pizzas c'est la vie</small>`;

document.querySelector('.newsContainer').setAttribute('style', '');

const closeButton = document.querySelector('.closeButton');
closeButton.addEventListener('click', function (event) {
	event.preventDefault();
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});
