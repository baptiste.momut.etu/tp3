import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = element.querySelector('form');

		form.addEventListener('submit', this.submit);
	}

	submit(event) {
		event.preventDefault();
		const input = document.querySelector('input[name=name]');
		console.log('Le formulaire a été soumis avec la valeur :' + input.value);
		if (!input.value) alert('Vous devez rentrer un nom de pizza');
		else {
			alert(`La pizza ${input.value} a été ajoutée`);
			input.value = '';
		}
	}
}
